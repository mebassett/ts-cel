import * as pz from "parzec";

/**
 * AST and Parser for the cell equations.
 * Things here are quite straightforward.
 *
 * The Parser comes directly from the example at
 * https://johtela.github.io/parzec/README.html
 * (Handy that its based on Haskell's Parsec!)
 */

export interface RawVal {
  kind: "rawval"
  value: number|string;
}
export interface CellVal {
  kind: "cellval"
  cellId: string;
}
export interface Add {
  kind: "add";
  addend1: CellEquation;
  addend2: CellEquation;
}
export interface Subtract {
  kind: "subtract";
  subtrahend: CellEquation;
  minuend: CellEquation;
}
export interface Multiply {
  kind: "multiply";
  multiplier: CellEquation;
  multiplicand: CellEquation;
}
export interface Divide {
  kind: "divide";
  dividend: CellEquation;
  divisor: CellEquation;
}

export type CellEquation = RawVal|CellVal|Add|Subtract|Multiply|Divide;

export enum ExprToken {
  Number,
  CellRef,
  OpenParen,
  CloseParen,
  Plus,
  Minus,
  Multiply,
  Divide,
  Whitespace,
  EOF
}

const lexer = new pz.Lexer<ExprToken>(
    [ /-?\d+(?:\.\d+)?(?:[eE][+-]?\d+)?/, ExprToken.Number ],
    [ /[a-zA-Z]+\d+/, ExprToken.CellRef ], [ /\(/, ExprToken.OpenParen ],
    [ /\)/, ExprToken.CloseParen ], [ /\+/, ExprToken.Plus ],
    [ /-/, ExprToken.Minus ], [ /\*/, ExprToken.Multiply ],
    [ /\//, ExprToken.Divide ], [ /[\t\n\r ]+/, ExprToken.Whitespace ]);

const optws = pz.terminal(ExprToken.Whitespace, "<whitespace>").optionalRef();
const number =
    pz.terminal(ExprToken.Number, "<number>")
        .map(t => ({kind : "rawval", value : Number(t.text)} as CellEquation))
        .followedBy(optws);
const cellRef =
    pz.terminal(ExprToken.CellRef, "<cellid>")
        .map(t => ({kind : "cellval", cellId : t.text.toUpperCase()} as
                   CellEquation))
        .followedBy(optws);
const openParen = pz.terminal(ExprToken.OpenParen, "(").followedBy(optws);
const closeParen = pz.terminal(ExprToken.CloseParen, ")").followedBy(optws);
const plus = pz.terminal(ExprToken.Plus, "+").followedBy(optws);
const minus = pz.terminal(ExprToken.Minus, "-").followedBy(optws);
const multiply = pz.terminal(ExprToken.Multiply, "*").followedBy(optws);
const divide = pz.terminal(ExprToken.Divide, "/").followedBy(optws);
const eof = pz.terminal(ExprToken.EOF, "<end of input>")

const addop = pz.operators(
    [
      plus, (a: CellEquation, b: CellEquation) =>
                ({kind : "add", addend1 : a, addend2 : b} as CellEquation)
    ],
    [
      minus,
      (a: CellEquation, b: CellEquation) =>
          ({kind : "subtract", minuend : a, subtrahend : b} as CellEquation)
    ]);
const mulop = pz.operators(
    [
      multiply, (a: CellEquation, b: CellEquation) =>
                    ({kind : "multiply", multiplier : a, multiplicand : b} as
                     CellEquation)
    ],
    [
      divide, (a: CellEquation, b: CellEquation) =>
                  ({kind : "divide", dividend : a, divisor : b} as CellEquation)
    ]);

const term = new pz.Ref<pz.Parser<CellEquation, pz.Token<ExprToken>>>();

const expr = pz.forwardRef(term).chainOneOrMore(addop);
const factor = expr.bracketedBy(openParen, closeParen).or(number).or(cellRef);
term.target = factor.chainOneOrMore(mulop);

const rootExpr = optws.seq(expr).followedBy(eof);

export const parseCellEquation = (cellValue: string): CellEquation => {
  // let's start with some base case rules about parsing.
  // 1. if we don't start with an equal sign, then its a raw value.
  if (cellValue.charAt(0) !== "=")
    return {kind : "rawval", value : cellValue};

  // 2. if its an equal sign followed by a number, its still a raw value.
  if (!Number.isNaN(Number(cellValue.substring(1))))
    return {kind : "rawval", value : Number(cellValue.substring(1))};

  return pz.parse(
      rootExpr, pz.lexerInput<ExprToken>(cellValue.substring(1), lexer,
                                         new pz.Token(ExprToken.EOF, "<end>")));
};
