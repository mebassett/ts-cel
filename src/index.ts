import {CellEquation, parseCellEquation} from './Parser';

function getDataFromStorage<T>(itemId: string, defaultVal: T): T {
  const storedData = window.localStorage.getItem(itemId);
  if (storedData) {
    return JSON.parse(storedData);
  } else
    return defaultVal;
}

/**
 * This Class is really two things (which is bad design, it should just be one!)
 * 1. It draws the DOM.
 * 2. It contains the local environment for the cell editor language.
 * 3. (off by one error, sorry) it handles ancillary stuff like saving the
 *    local environment and adding columns.

 * We parse each cell's contents into an AST (via the Parser module) and then
 * intepret that AST wrt the environment stored in `rawData`.
 * SpreadsheetTable.computeVals will intepret each cell, and
 * SpreadSheetTable.createInputElem will ensure that each cell triggers it when
 * its changed.
 * SpreadSheetTable.evaluateCellEquation does the actual evaluation.  It
 requires
 * a cell id to detect self-referencing loops.

 * The constructor expects a DOM id of a table element.
 * It will throw exceptions otherwise.
 */
class SpreadsheetTable {
  rawData: Record<string, string>;
  inputElems: HTMLInputElement[];
  theTable: HTMLDivElement;
  numRows: number;
  numCols: number;

  // This mostly just draws the DOM.
  constructor(elementId: string) {
    this.rawData = getDataFromStorage('rawData', {} as Record<string, string>);
    this.numRows = getDataFromStorage('numRows', 5);
    this.numCols = getDataFromStorage('numCols', 5);

    this.inputElems = [];
    const elem = document.getElementById(elementId);
    if (elem === null)
      throw `Cannot find element: ${elementId}`;
    if (!(elem instanceof HTMLDivElement))
      throw `The element is not a div: ${elementId}`;

    this.theTable = elem;

    if (elem.parentElement) {
      const addRowBtn = document.createElement("button") as HTMLButtonElement
      addRowBtn.innerHTML = "add row";
      addRowBtn.onclick = this.addRow;
      elem.parentElement.appendChild(addRowBtn);

      const addColBtn = document.createElement("button") as HTMLButtonElement
      addColBtn.innerHTML = "add col";
      addColBtn.onclick = this.addCol;
      elem.parentElement.appendChild(addColBtn);

      const saveBtn = document.createElement("button") as HTMLButtonElement
      saveBtn.innerHTML = "save";
      saveBtn.onclick = this.save;
      elem.parentElement.appendChild(saveBtn);
    }

    for (let rowNum = 0; rowNum <= this.numRows; rowNum++) {
      for (let colNum = 0; colNum <= this.numCols; colNum++) {
        const newCell = document.createElement("div");
        this.theTable.appendChild(newCell);
        const cssStyle = `grid-column: ${(colNum + 1).toString()}; grid-row: ${
            (rowNum + 1).toString()};`;
        newCell.style.cssText = cssStyle;

        const letter = String.fromCharCode("A".charCodeAt(0) + colNum - 1);
        if (rowNum === 0 && colNum === 0) {
          newCell.innerHTML = "";
          newCell.classList.add("header");
        } else if (rowNum === 0) {
          newCell.innerHTML = letter;
          newCell.classList.add("header");
        } else if (colNum === 0) {
          newCell.innerHTML = rowNum.toString();
          newCell.classList.add("header");
        } else {
          const cellId = letter + rowNum.toString();
          const cellEditor = this.createInputElem(cellId);
          this.inputElems.push(cellEditor);
          newCell.appendChild(cellEditor);
        }
      }
    }
    this.computeVals();
  }

  private evaluateCellEquation = (rootId: string,
                                  cellEq: CellEquation): number|string => {
    switch (cellEq.kind) {
    case "rawval":
      if (!Number.isNaN(Number(cellEq.value)) && cellEq.value !== '')
        return Number(cellEq.value);
      else
        return cellEq.value;
      break;
    case "cellval":
      if (cellEq.cellId === rootId)
        return "RECUR ERROR";
      return this.evaluateCellEquation(
          rootId, parseCellEquation(this.rawData[cellEq.cellId]));
      break;
    case "add":
      let addend1 = this.evaluateCellEquation(rootId, cellEq.addend1);
      let addend2 = this.evaluateCellEquation(rootId, cellEq.addend2);
      if (typeof addend1 === "number" && typeof addend2 === "number")
        return addend1 + addend2;
      else
        return "ADD ERROR";
      break;
    case "subtract":
      let minuend = this.evaluateCellEquation(rootId, cellEq.minuend);
      let subtrahend = this.evaluateCellEquation(rootId, cellEq.subtrahend);
      if (typeof minuend === "number" && typeof subtrahend === "number")
        return minuend - subtrahend;
      else
        return "SUBTRACT ERROR";
      break;
    case "divide":
      let dividend = this.evaluateCellEquation(rootId, cellEq.dividend);
      let divisor = this.evaluateCellEquation(rootId, cellEq.divisor);
      if (typeof dividend === "number" && typeof divisor === "number")
        return dividend / divisor;
      else
        return "DIVIDE ERROR";
      break;
    case "multiply":
      let multiplier = this.evaluateCellEquation(rootId, cellEq.multiplier);
      let multiplicant = this.evaluateCellEquation(rootId, cellEq.multiplicand);
      if (typeof multiplier === "number" && typeof multiplicant === "number")
        return multiplier * multiplicant;
      else
        return "MUL ERROR";
      break;
    }
  };

  private createInputElem = (elemId: string): HTMLInputElement => {
    const elem = document.createElement("input") as HTMLInputElement;
    elem.value = "";
    elem.type = "text";
    elem.id = elemId;
    elem.onfocus = (e) => {
      if (e.target && e.target instanceof HTMLInputElement)
        e.target.value =
            e.target.id in this.rawData ? this.rawData[e.target.id] : '';
    };

    elem.onblur = (e) => {
      if (e.target && e.target instanceof HTMLInputElement)
        this.rawData[e.target.id] = e.target.value;
      this.computeVals();
    };
    return elem;
  };

  private computeVals = (): void => {
    this.inputElems.forEach((elem) => {
      const rawVal = elem.id in this.rawData ? this.rawData[elem.id] : '';
      const parsedAst = parseCellEquation(rawVal);
      const val = this.evaluateCellEquation(elem.id, parsedAst);
      elem.value = typeof val === 'number' ? val.toString() : val;
    });
  };

  private addRow = (e: Event): void => {
    this.numRows = this.numRows + 1;
    for (let colNum = 0; colNum <= this.numCols; colNum++) {
      const newCell = document.createElement("div");
      this.theTable.appendChild(newCell);
      const cssStyle = `grid-column: ${(colNum + 1).toString()}; grid-row: ${
          (this.numRows + 1).toString()};`;
      newCell.style.cssText = cssStyle;
      const letter = String.fromCharCode("A".charCodeAt(0) + colNum - 1);
      if (colNum === 0) {
        newCell.innerHTML = this.numRows.toString();
        newCell.classList.add('header');
      } else {
        const cellId = letter + this.numRows.toString();
        const cellEditor = this.createInputElem(cellId);
        this.inputElems.push(cellEditor);
        newCell.appendChild(cellEditor);
      }
    }
  };

  private addCol = (e: Event): void => {
    this.numCols = this.numCols + 1;
    for (let rowNum = 0; rowNum <= this.numRows; rowNum++) {
      const newCell = document.createElement("div");
      this.theTable.appendChild(newCell);
      const cssStyle =
          `grid-column: ${(this.numCols + 1).toString()}; grid-row: ${
              (rowNum + 1).toString()};`;
      newCell.style.cssText = cssStyle;
      const letter = String.fromCharCode("A".charCodeAt(0) + this.numCols - 1);
      if (rowNum === 0) {
        newCell.innerHTML = letter;
        newCell.classList.add('header');
      } else {
        const cellId = letter + rowNum.toString();
        const cellEditor = this.createInputElem(cellId);
        this.inputElems.push(cellEditor);
        newCell.appendChild(cellEditor);
      }
    }
  };

  private save = (e: Event): void => {
    window.localStorage.setItem('rawData', JSON.stringify(this.rawData));
    window.localStorage.setItem('numCols', JSON.stringify(this.numCols));
    window.localStorage.setItem('numRows', JSON.stringify(this.numRows));
  };
};

const app = new SpreadsheetTable("theTable");
