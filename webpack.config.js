const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const TerserPlugin = require('terser-webpack-plugin');

const PROD = 'production';
const DEV = 'dev';
const TARGET_ENV = process.env.npm_lifecycle_event === 'build' ? PROD : DEV;

const entryPath = path.join(__dirname, './src/index.ts');
const outputPath = path.join(__dirname, './dist');
const outputFilename = TARGET_ENV === PROD ? '[name]-[hash].js' : '[name].js';

const commonConfig = {
  output : {path : outputPath, filename : `${outputFilename}`},
  resolve :
      {extensions : [ '.js', '.ts', '.tsx' ], modules : [ 'node_modules' ]},
  module : {
    rules : [
      {
        test : /\.(eot|ttf|woff|woff2|svg)$/,
        use : 'file-loader?publicPath=../../&name=[hash].[ext]'
      },
      {
        test : /\.tsx?$/,
        use : 'ts-loader',
        exclude : /node_modules/,
      }
    ]
  },
  plugins : [
    new webpack.LoaderOptionsPlugin({options : {postcss : [ autoprefixer() ]}}),
    new HtmlWebpackPlugin(
        {template : './index.html', inject : 'body', filename : 'index.html'})
  ]
};

if (TARGET_ENV === DEV) {
  module.exports = merge(commonConfig, {
    devtool : "source-map",
    entry : [ 'webpack-dev-server/client?http://localhost:8080', entryPath ],
    devServer : {
      historyApiFallback : true,
      contentBase : './dist',
      hot : true,
      proxy : {
        '/game/**' : {target : 'http://localhost:8081'},
        '/game' : {target : 'ws://localhost:8081', ws : true}
      }

    },
  });
}

if (TARGET_ENV === PROD) {
  module.exports = merge(commonConfig, {
    entry : entryPath,
    optimization : {
      minimizer : [
        // https://elm-lang.org/0.19.0/optimize
        new TerserPlugin({
          terserOptions : {
            mangle : false,
            compress : {
              pure_funcs : [
                'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9', 'A2', 'A3',
                'A4', 'A5', 'A6', 'A7', 'A8', 'A9'
              ],
              pure_getters : true,
              keep_fargs : false,
              unsafe_comps : true,
              unsafe : true,
            }
          },
          extractComments : false
        }),
        new TerserPlugin(
            {terserOptions : {mangle : true}, extractComments : false})
      ]
    },

  });
}
